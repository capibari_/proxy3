<?php

use yii\bootstrap4\Html;

?>

<div class="container-fluid">
    <?= Html::a('Send Config', ['/proxy/send-config/', 'id' => $model['id']], ['class' => 'btn btn-secondary']);?>
    <?= Html::a('Restart Service', ['/proxy/restart-service/', 'id' => $model['id']], ['class' => 'btn btn-secondary']);?>
    <?= Html::a('Send Cron', ['/proxy/send-cron/', 'id' => $model['id']], ['class' => 'btn btn-secondary']);?>
    <?= Html::a('Restart Cron', ['/proxy/restart-cron/', 'id' => $model['id']], ['class' => 'btn btn-secondary']);?>
    <hr>
    <div class="row">
        <div class="col-auto col-lg-3">
            <pre><?=$model['config'];?></pre>
        </div>
        <div class="col-auto col-lg-9">
            <pre><?=$model['cron'];?></pre>
        </div>
    </div>

</div>