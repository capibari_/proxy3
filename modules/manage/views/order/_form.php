<?php

use app\components\RandomString;
use app\models\Port;
use app\models\User;
use kartik\date\DatePicker;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $m_rules */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="form-row align-items-center">
    <?= $form->field($model, 'user_id', ['options' => ['class' => 'form-group col-auto']])->

    dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'username'), ['class' => 'form-control mb-2']) ?>

    <?= $form->field($model, 'port_id', ['options' => ['class' => 'form-group col-auto']])->
    dropDownList(ArrayHelper::map(Port::find()->all(), 'id', 'number'), ['class' => 'form-control mb-2']) ?>

    <?= $form->field($model, 'is_activate', ['options' => ['class' => 'form-group col-auto mt-2', 'name' => 'status_'.$model->id]])
        ->widget(SwitchInput::classname(), [
            'name' => 'status_'.$model->id,
            'pluginOptions' => [
                'size' => 'medium',
                'onColor' => 'success',
                'offColor' => 'danger',
            ]
        ]); ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-4']) ?>
    </div>
</div>
<div class="form-row align-items-center">
    <?= $form->field($model, 'token', ['options' => ['class' => 'form-group col-auto']])->textInput(['maxlength' => true, 'value'=>RandomString::generate(8)]) ?>

    <?= $form->field($model, 'activate', ['options' => ['class' => 'form-group col-4', 'name' => 'activate_'.$model->id]])
        ->widget(DatePicker::classname(), [
            'name' => 'activate',
            'options' => [
                    'placeholder' => 'Select issue date ...',
                    'value' => Yii::$app->formatter->asDate($model->activate, 'dd.MM.yyyy'),
            ],
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy',
                'todayHighlight' => true
            ],
        ]); ?>

    <?= $form->field($model, 'end', ['options' => ['class' => 'form-group col-4', 'name' => 'end_'.$model->id]])
        ->widget(DatePicker::classname(), [
            'name' => 'end',
            'options' => [
                'placeholder' => 'Select issue date ...',
                'value' => Yii::$app->formatter->asDate($model->end, 'dd.MM.yyyy'),
            ],
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy',
                'todayHighlight' => true
            ],
        ]); ?>
</div>
<div class="container">
    <div class="row">
        <div class="col-4">
            <div class="row align-items-center">
                <h5>Rules</h5>
                <?= Html::Button('Add', ['id' => 'add', 'class' => 'btn btn-outline-dark ml-3']) ?>
                <?= Html::Button('Remove', ['id' => 'remove', 'class' => 'btn btn-outline-dark ml-3']) ?>
            </div>
            <div class="rules pt-3"></div>
        </div>
        <div class="col">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

