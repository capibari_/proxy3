<?php


use app\components\RandomString;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;


$this->registerJsFile(
    '@web/js/order/generate.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

/* @var $this yii\web\View */
/* @var $m_rules */
/* @var $model app\models\GenerateModel */
/* @var $user app\models\User */
/* @var $order app\models\Order */
/* @var $form \yii\widgets\ActiveForm */
?>
    <?php foreach(Yii::$app->session->getAllFlashes() as $key => $message):?>
        <div class="alert alert-warning alert-dismissible" role="alert">
        <div class="flash-<?=$key?>"><?=$message?></div>
        </div>
    <?php endforeach; ?>

<div class="order-form container-fluid">
    <?php $form = ActiveForm::begin(); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-sm-auto">
                <div class="row status">
                    <div class="col" id="avalible_port"></div>
                    <div class="col" id="error_status"></div>
                </div>
                <div class="form-row align-items-center">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-3', 'id'=>'sendForm']) ?>
                    <?= $form->field($user, 'username', ['options' => ['class' => 'form-group col-auto']])->dropDownList($user->userList()) ?>
                    <?= $form->field($user, 'token', ['options' => ['class' => 'form-group col-auto']])->textInput(['value' => RandomString::generate(8)])?>
                    <?= $form->field($user, 'email', ['options' => ['class' => 'form-group col-auto']])->textInput()?>
                    <?= $form->field($user, 'user_ip', ['options' => ['class' => 'form-group col-auto']])->textInput()?>
                </div>
                <div class="form-row align-items-center">
                    <?= $form->field($model, 'multiple', ['options' => ['class' => 'form-group col-auto']])->textInput(['type' => 'number', 'min'=>'1', 'value'=>'0']);?>
                    <?= $form->field($order, 'proxy_type', ['options' => ['class' => 'form-group col-auto']])->dropDownList(['proxy'=>'proxy', 'socks'=>'socks']) ?>
                    <?= $form->field($order, 'auth_type', ['options' => ['class' => 'form-group col-auto']])->dropDownList(['login'=>'login', 'ip'=>'ip']) ?>
                    <?= $form->field($order, 'activate', ['options' => ['class' => 'form-group col-auto', 'name' => 'activate']])
                        ->widget(DateTimePicker::classname(), [
                            'name' => 'date',
                            'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                            'options' => [
                                    'placeholder' => 'Select date..',
                                    'value' => date('d-m-Y H:i',time()),
                                ],
                            'pluginOptions' => [
                                'format' => 'dd M yyyy hh:ii',
                                'startDate'=> date('d-m-Y H:i',time()),
                            ],
                        ]); ?>
                    <?= $form->field($order, 'end', ['options' => ['class' => 'form-group col-auto']])->textInput(['type' => 'number', 'min'=>'1', 'value'=>'30']);?>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-4">
                                    Rules
                                    <?= Html::Button('Add', ['id' => 'add', 'class' => 'btn btn-outline-dark ml-1 mb-2']) ?>
                                    <?= Html::Button('Remove', ['id' => 'remove', 'class' => 'btn btn-outline-dark ml-1 mb-2']) ?>
                                <div class="rules"></div>
                            </div>
                            <div class="col-4">
                                <?= $form->field($model, 'ports', ['options' => ['class' => 'form-group col-auto']])
                                    ->dropDownList(ArrayHelper::map([], 'id', 'number'), ['class' => 'form-control mb-2 col', 'multiple' => true, 'size'=>6])
                                    ->label(false)?>
                            </div>
                            <div class="col-4 container-fluid">
                                <div class="row">
                                    <?= $form->field($model, 'distribute')->checkBox(['label' => 'distribute','data-size'=>'small', 'class'=>'rules-checkbox', 'id'=>'distribute']) ?>
                                    <?= $form->field($model, 'in_use')->checkBox(['label' => 'free','data-size'=>'small', 'checked'=>true, 'class'=>'rules-checkbox', 'id'=>'in_use']) ?>
                                </div>
                                <div class="row">
                                    <?= $form->field($model, 'provider', ['options' => ['class' => 'form-group col-auto']])->dropDownList($model->provider, ['id'=>'provider']) ?>
                                    <?= $form->field($model, 'reconnect_time')->dropDownList($model->reconnect_time, ['id'=>'reconnect']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?= $form->field($order, 'description')->textarea(['rows' => 6]) ?>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <div class="row">
                        <div class="col" id="ordersInfo">
                            <?php if($orderList):?>
                                <?php foreach ($orderList as $id => $order):?>
                                    <?=$order?><br>
                                <?php endforeach;?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
    </div>
    <?php ActiveForm::end(); ?>
</div>