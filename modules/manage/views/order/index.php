<?php

use app\models\Server;
use app\models\Firewall;
use app\models\Modem;
use app\models\Order;
use app\models\Rules;
use app\models\User;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\OrderSearch */

$this->title = 'Order List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index container-fluid">
    <p>
        <?= Html::a('Create', ['generate'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('List', ['list'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    $dataProvider->pagination  = false;
    $gridColumns = [
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return \kartik\grid\GridView::ROW_COLLAPSED;
            },
            'detailUrl'=>Url::to(['/manage/order/update', 'id' => $model->id]),
            'detail' => function ($model, $key, $index, $column) {
                $order = Order::find()->where(['id'=>$model['id']])->one();
                $m_rules = Firewall::find()->where(['order_id' => $order['id']])->all();
                $m_rules = $m_rules ? $m_rules : new Rules();
                return Yii::$app->controller->renderPartial('_expand-row-details', [
                    'model' => $order,
                    'm_rules' => $m_rules,
                    'data' => $model,
                    'key' => $key
                ]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'width' => '1px',
            'headerOptions' => ['class' => 'kartik-sheet-style'],
        ],
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'is_activate',
            'vAlign' => 'middle',
            'width' => 'auto',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Order::$statusName,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Status'],
        ],
        [
            'attribute' => 'serverName',
            'label' => 'Server',
            'vAlign' => 'middle',
            'width' => '200px',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Server::getServernameAsArray(),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Server'],
        ],
        [
            'attribute' => 'modem',
            'vAlign' => 'middle',
            'width' => '150px',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Modem::getModemListAsArray(),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Modem'],
        ],
        [
            'attribute' => 'portNumber',
            'label' => 'Port',
            'vAlign' => 'middle',
            'width' => '100px',
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
        ],
        [
            'attribute' => 'username',
            'vAlign' => 'middle',
            'width' => '150px',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => User::getUsernameAsArray(),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'User'],
        ],
        [
            'attribute' => 'token',
            'vAlign' => 'middle',
            'width' => 'auto',
        ],
        [
            'attribute' => 'email',
            'vAlign' => 'middle',
            'width' => '200px',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => User::getEmailAsArray(),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Email'],
        ],
        [
            'attribute' => 'ruleList',
            'vAlign' => 'middle',
            'width' => '200px',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Rules::getRuleListAsArray(),
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Rules'],
        ],
        [
            'attribute' => 'activate',
            'vAlign' => 'middle',
            'width' => 'auto',
        ],
        [
            'attribute' => 'end',
            'vAlign' => 'middle',
            'width' => 'auto',
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'width' => '100px',
            'dropdownOptions' => ['class' => 'float-right'],
            'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
            'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
            'deleteOptions' => ['title' => 'Delete', 'data-toggle' => 'tooltip'],
            'headerOptions' => ['class' => 'kartik-sheet-style'],
        ],
    ];?>
    <?= GridView::widget([
        'id' => 'grid-table',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'toolbar' => [
            [
                'content'=>
                    Html::a('List selected', ['#'], [
                        'class' => 'btn btn-primary align-items-start justify-content-end',
                        'onclick' => 'list = $("#grid-table").yiiGridView("getSelectedRows");
                                    window.location.href = "/web/manage/order/list?orders="+list;',
                    ]).Html::a('Delete selected', ['#'], [
                        'class' => 'btn btn-danger align-items-start justify-content-end',
                        'onclick' => 'list = $("#grid-table").yiiGridView("getSelectedRows");
                                    model = "Order";
                                    $.post("/ajax/delete?list="+list+"&model="+model,
                                     function(data){console.log(data);}
                                    );',
                    ]),
            ],
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => GridView::TYPE_DEFAULT,
            'heading' => $this->title,
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10],
        'itemLabelSingle' => 'port',
        'itemLabelPlural' => 'ports'
    ]);?>


</div>
