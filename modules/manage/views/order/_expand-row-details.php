<?php

use app\models\Port;
use app\models\User;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use kartik\dialog\Dialog;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $key */
/* @var $m_rules */
/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form container-fluid">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/manage/order/update','model' => $model::className(), 'id' => $model->id]),
        'options' => ['method' => 'post', 'data-pjax' => true, 'onsubmit' => 'false'],
    ]); ?>

    <div class="d-flex flex-row align-items-center">
        <h5 class="">Order # <?= $model->id;?></h5>
    </div>
    <div class="form-row align-items-center">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-3 ml-2']) ?>
        <?= $form->field($model, 'is_activate', ['options' => ['class' => 'form-group col-auto mt-3', 'name' => 'status_'.$model->id]])
            ->widget(SwitchInput::classname(), [
                'options' => [
                    'id' => 'status_'.$model->id,
                ],
                'pluginOptions' => [
                    'size' => 'medium',
                    'onColor' => 'success',
                    'offColor' => 'danger',
                ]
            ]); ?>

        <?= $form->field($model, 'user_id', ['options' => ['class' => 'form-group col-auto']])->
        dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'username'), ['class' => 'form-control']) ?>

        <?= $form->field($model, 'token', ['options' => ['class' => 'form-group col-auto']])->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'activate', ['options' => ['class' => 'form-group col-auto', 'name' => 'date']])
            ->widget(DateTimePicker::classname(), [
                'name' => 'date',
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'Select date..'],
                'pluginOptions' => [
                    'format' => 'dd M yyyy hh:ii',
                    'startDate'=> date('d m Y H:i',time()),
                ],
            ]); ?>

        <?= $form->field($model, 'end', ['options' => ['class' => 'form-group col-auto', 'name' => 'date']])
            ->widget(DateTimePicker::classname(), [
                'name' => 'date',
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'options' => ['placeholder' => 'Select date..'],
                'pluginOptions' => [
                    'format' => 'dd M yyyy hh:ii',
                    'startDate'=> date('d m Y H:i',time()),
                ],
            ]); ?>

    </div>
    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
    <?php ActiveForm::end(); ?>
</div>