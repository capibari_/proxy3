<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\firewall */

$this->title = 'Create Allow';
$this->params['breadcrumbs'][] = ['label' => 'Allows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rules-create container-fluid">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
