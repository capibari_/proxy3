<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $key */
/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rules-form container-fluid">

    <?php $form = ActiveForm::begin([
            'action' => Url::to(['/manage/rules/update','model' => $model::className(), 'id' => $model->id]),
            'options' => ['method' => 'post', 'data-pjax' => true, 'onsubmit' => 'false'],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'permission')->dropDownList([0=>'deny',1=>'allow']);?>
    <?= $form->field($model, 'path')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'use_limit')->textInput(['type' => 'number', '']) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>