<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\firewall */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rules-form container-fluid">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'path')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'use_limit')->textInput(['type' => 'number', '']) ?>
    <?= $form->field($model, 'permission')->dropDownList([0=>'deny',1=>'allow']);?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
