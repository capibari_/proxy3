<?php

use app\models\Modem;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index container-fluid">
    <p>
        <?= Html::a('Add User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    $dataProvider->pagination  = false;
    $gridColumns = [
        [
            'class'=>'kartik\grid\SerialColumn',
            'contentOptions'=>['class'=>'kartik-sheet-style'],
            'width'=>'36px',
            'pageSummary'=>'Total',
            'pageSummaryOptions' => ['colspan' => 6],
            'header'=>'',
            'headerOptions'=>['class'=>'kartik-sheet-style']
        ],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            // uncomment below and comment detail if you need to render via ajax
            'detailUrl'=>Url::to(['/admin/user/update', 'id' => $model->id]),
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand-row-details', [
                    'model' => $model,
                    'key' => $key
                ]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'width' => '1px',
            'headerOptions' => ['class' => 'kartik-sheet-style'],
        ],
        [
            'attribute' => 'id',
            'vAlign' => 'middle',
            'width' => '1px',
            'value' => $model->id,
        ],
        [
            'attribute' => 'username',
            'vAlign' => 'middle',
            'width' => 'auto',
            'value' => $model->username,
        ],
        [
            'attribute' => 'email',
            'vAlign' => 'middle',
            'width' => 'auto',
            'value' => $model->email,
        ],
        [
            'class' => 'kartik\grid\BooleanColumn',
            'attribute' => 'status',
            'vAlign' => 'middle'
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => false,
            'width' => '100px',
            'dropdownOptions' => ['class' => 'float-right'],
            'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
            'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
            'deleteOptions' => ['title' => 'Delete', 'data-toggle' => 'tooltip'],
            'headerOptions' => ['class' => 'kartik-sheet-style'],
        ],
    ];?>
    <?= GridView::widget([
        'id' => 'grid-table',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto '],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'toolbar' => [
            [
                'content'=>
                    Html::a('Delete selected', ['#'], [
                        'class' => 'btn btn-success align-items-start justify-content-end',
                        'onclick' => 'list = $("#grid-table").yiiGridView("getSelectedRows");
                                    model = "User";
                                    $.post("/ajax/delete?list="+list+"&model="+model,
                                     function(data){console.log(data);}
                                    );',
                    ]),
            ],
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => GridView::TYPE_DEFAULT,
            'heading' => $this->title,
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10],
        'itemLabelSingle' => 'user',
        'itemLabelPlural' => 'users'
    ]);?>
</div>
