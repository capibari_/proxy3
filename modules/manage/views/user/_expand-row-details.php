<?php

use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $key */
/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form container-fluid">
    <h5><?= $model->username;?></h5>
    <?php $form = ActiveForm::begin([
            'action' => Url::to(['/manage/user/update', 'id' => $model->id]),
            'options' => ['method' => 'post', 'data-pjax' => true, 'onsubmit' => 'false', ],
    ]); ?>
    <div class="form-row align-items-center">
        <?= $form->field($model, 'status', ['options' => ['class' => 'form-group col-auto mb-0', 'name' => 'status_'.$model->id]])
            ->widget(SwitchInput::classname(), [
                    'options' => [
                        'id' => 'status_'.$model->id,
                    ],
                    'name' => 'status_'.$model->id,
                    'pluginOptions' => [
                            'size' => 'medium',
                            'onColor' => 'success',
                            'offColor' => 'danger',
                        ]
            ]); ?>
        <?= $form->field($model, 'email', ['options' => ['class' => 'form-group col-auto']])->textInput(['maxlength' => true]) ?>
        <div class="col-auto">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-3']) ?>
        </div>
    </div>
    <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
    <?php ActiveForm::end(); ?>
</div>