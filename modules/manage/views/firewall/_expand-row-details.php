<?php

use app\models\Rules;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $key */
/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="firewall-form container-fluid">
    <?php $form = ActiveForm::begin([
            'action' => Url::to(['/admin/ajax/update', 'model' => $model::className(), 'id' => $model->id]),
            'options' => ['method' => 'post', 'data-pjax' => true, 'onsubmit' => 'false', ],
    ]); ?>
    <div class="form-row align-items-center">
        <?= $form->field($model, 'rule_id', ['options' => ['class' => 'form-group']])->
        dropDownList(ArrayHelper::map(Rules::find()->all(), 'id', 'name'), ['class' => 'form-control']) ?>
        <div class="col-auto">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-3']) ?>
        </div>
    </div>
    <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
    <?php ActiveForm::end(); ?>
</div>