<?php

use app\models\Order;
use app\models\Rules;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rules */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'order_id', ['options' => ['class' => 'form-group']])->
dropDownList(ArrayHelper::map(Order::find()->all(), 'id', 'id'), ['class' => 'form-control mb-2']) ?>
<?= $form->field($model, 'rule_id', ['options' => ['class' => 'form-group']])->
dropDownList(ArrayHelper::map(Rules::find()->all(), 'id', 'name'), ['class' => 'form-control mb-2']) ?>
<?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>

