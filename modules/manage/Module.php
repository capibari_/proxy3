<?php

namespace app\modules\manage;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{

    public $layout = '/manage';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\manage\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
