<?php

namespace app\modules\manage\controllers;

use app\components\Proxy;
use app\models\Server;
use app\models\Firewall;
use app\models\Rules;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

class MainController extends \app\controllers\DefaultController
{
    public function actionIndex()
    {
        $array = [];
        $keys = [];
        $server_query = Server::find()->all();
        $server_list = ArrayHelper::toArray($server_query, [
            'app\models\Modem' => [
                'id',
                'ip',
                'ext_ip',
                'config'
            ],
        ]);

        foreach ($server_list as $server){
            $proxy = new Proxy($server['id']);
            array_push($keys, $server['id']);
            array_push($array, [
                'id' => $server['id'],
                'ip' => $server['ext_ip'],
                'config' => $proxy->getConfig(),
                'cron' => $proxy->getCron()
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $array,
            'keys' => $keys,
            'sort' => ['attributes' =>['id', 'ip']],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

}
