<?php

namespace app\modules\manage\controllers;

use app\components\Debug;
use app\components\Proxy;
use app\components\RandomString;
use app\components\Stringer;
use app\models\Firewall;
use app\models\GenerateModel;
use app\models\OrderSearch;
use app\models\Port;
use app\models\Rules;
use app\models\User;
use Yii;
use app\models\Order;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class OrderController extends \app\controllers\DefaultController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 1000;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionList($orders = null)
    {
        $list = '<pre>';
        if($orders){
            $orders = explode(',', $orders);
            foreach($orders as $order){
                $order = $this->getOrder($order);
                $list .= $this->getResult($order);
            }
        } else {
            foreach (Order::find()->asArray()->all() as $order){
                $list .= $this->getResult($order);
            }
        }

        return $this->render('list',[
            'list' => $list
        ]);
    }

    public function actionGenerate()
    {
        $m_gen = new GenerateModel();
        $m_user = new User();
        $m_order = new Order();
        $orderList = [];

        if($post = Yii::$app->request->post()){
            if(!$post['User']['username']){
                $post['User']['username'] = RandomString::generate(8);
                $post['User']['status'] = 1;
            }
            if(($m_user->load($post) && $m_user->save())){
                $post['Order']['user_id'] = $m_user->id;
            }

            if($post['GenerateModel']['multiple'] == count($post['GenerateModel']['ports'])){
                foreach ($post['GenerateModel']['ports'] as $port){
                    $m_order = new Order();
                    $post['Order']['port_id'] = $port;
                    $post['Order']['is_activate'] = 1;
                    if($m_order->load($post) && $m_order->save()){
                        $orderList[$m_order->id] = Stringer::port([
                            'ip' => $m_order->port->server->ext_ip,
                            'port' => $m_order->port->number,
                            'user' => $m_user->username,
                            'token' => $m_user->token
                        ]);
                    }
                    if ($post['Rules']) {
                        foreach ($post['Rules'] as $rule) {
                            $m_fwall = new Firewall();
                            if(!$rule) continue;
                            $post['Firewall']['order_id'] = $m_order->id;
                            $post['Firewall']['rule_id'] = $rule;
                            $m_fwall->load($post);
                            $m_fwall->save();
                        }
                    }
                }
            }

            $proxy = new Proxy($m_order->port->server->id);
            $proxy->sendConfig();
            $proxy->restartProxy();

            return $this->render('generate', [
                'model' => $m_gen,
                'user' => $m_user,
                'order' => $m_order,
                'orderList' => $orderList,
            ]);
        }

        return $this->render('generate', [
            'model' => $m_gen,
            'user' => $m_user,
            'order' => $m_order,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    private function getOrder($id)
    {
        return Order::find()->where(['id' => $id])->asArray()->one();
    }

    private function getResult($order)
    {
        $port = Port::find()->where(['id' => $order['port_id']])->with('server')->asArray()->one();
        $user = User::find()->where(['id' => $order['user_id']])->asArray()->one();

        return Stringer::port([
            'ip' => $port['server']['ext_ip'],
            'port' => $port['number'],
            'user' => $user['username'],
            'token' => $user['token']
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
