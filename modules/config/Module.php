<?php

namespace app\modules\config;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{

    public $layout = '/config';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\config\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }
}
