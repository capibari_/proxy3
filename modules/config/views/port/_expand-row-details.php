<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $key */
/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $modem array */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="port-form container-fluid">

    <h5>Port # <?= $model->number;?></h5>
    <?php
    $form = ActiveForm::begin([
        'action' => Url::to(['/config/port/update','model' => $model::className(), 'id' => $model->id]),
        'options' => ['method' => 'post', 'data-pjax' => true, 'onsubmit' => 'false'],
    ]); ?>
    <div class="form-row align-items-center">
        <div class="form-row">
            <?= $form->field($model, 'modem_id', ['options' => ['class' => 'form-group col-auto ml-3']])->
            dropDownList([$modem['id'] => $modem['ip']], ['class' => 'form-control mb-2']) ?>
        </div>
        <div class="col-auto">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-2']) ?>
        </div>

    </div>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?php ActiveForm::end(); ?>

</div>