<?php

use app\models\Modem;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Port */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="port-form container-fluid">
    <?php $form = ActiveForm::begin(); ?>
    <div class="form-row align-items-center">
        <div class="form-row">
        <?= $form->field($model, 'number', ['options' => ['class' => 'form-group col-auto']])->textInput(['class' => 'form-control mb-2']) ?>
        <div class="form-group col-auto field-port-range">
            <label for="port-range">Range</label>
            <input type="text" id="port-range" class="form-control mb-2" name="range" aria-required="true">
        </div>
        <?= $form->field($model, 'modem_id', ['options' => ['class' => 'form-group col-auto']])->
        dropDownList(ArrayHelper::map(Modem::find()->all(), 'id', 'ip', 'server.ext_ip'), ['class' => 'form-control mb-2']) ?>
        </div>
        <div class="col-auto">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-2']) ?>
        </div>

    </div>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php ActiveForm::end(); ?>
</div>
