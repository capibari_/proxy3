<pre>
<?php

use yii\helpers\Html;

foreach ($data as $server => $modems){
    echo '<ul>'.$server.'(Modems: '.count($modems).')';
    foreach ($modems as $modem => $ports){
        echo '<li>'.$modem.'(Ports: '.count($ports).')</li>';
    }
    echo '</ul>';
}