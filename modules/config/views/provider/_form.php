<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $form \yii\widgets\ActiveForm */
?>

<div class="provider-form container-fluid">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput()?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
