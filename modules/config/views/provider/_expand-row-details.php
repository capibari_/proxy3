<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;


/* @var $key */
/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $form \yii\widgets\ActiveForm */
?>

<div class="provider-form container-fluid">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/config/provider/update', 'model' => $model::className(), 'id' => $model->id]),
        'options' => ['method' => 'post', 'data-pjax' => true, 'onsubmit' => 'false', ],
    ]); ?>
    <div class="form-row align-items-center">
        <?= $form->field($model, 'name')->textInput()?>
        <div>
            <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-3']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>