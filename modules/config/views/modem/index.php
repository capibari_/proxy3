<?php

use app\models\Modem;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modem List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modem-index">
    <p>
        <?= Html::a('Add Modem', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    $dataProvider->pagination  = false;
    $gridColumns = [
            [
                'class'=>'kartik\grid\SerialColumn',
                'contentOptions'=>['class'=>'kartik-sheet-style'],
                'width'=>'36px',
                'pageSummary'=>'Total',
                'pageSummaryOptions' => ['colspan' => 6],
                'header'=>'',
                'headerOptions'=>['class'=>'kartik-sheet-style']
            ],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'width' => '50px',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                // uncomment below and comment detail if you need to render via ajax
                'detailUrl'=>Url::to(['/admin/modem/update', 'id' => $model->id]),
                'detail' => function ($model, $key, $index, $column) {
                    return Yii::$app->controller->renderPartial('_expand-row-details', [
                            'model' => $model['model'][$index],
                            'server' => $model['server'],
                            'key' => $key
                    ]);
                },
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true
            ],
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'width' => '1px',
                'headerOptions' => ['class' => 'kartik-sheet-style'],
            ],
            [
                'attribute' => 'id',
                'vAlign' => 'middle',
                'width' => '10px',
            ],
            [
                'attribute' => 'ip',
                'vAlign' => 'middle',
                'width' => 'auto',
            ],
            [
                'attribute' => 'server_name',
                'vAlign' => 'middle',
                'width' => 'auto',
            ],
        [
                'attribute' => 'provider',
                'vAlign' => 'middle',
                'width' => 'auto',
            ],
            [
                'attribute' => 'server_ip',
                'vAlign' => 'middle',
                'width' => 'auto',
            ],
        [
                'attribute' => 'reconnect',
                'vAlign' => 'middle',
                'width' => 'auto',
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => false,
                'width' => '100px',
                'dropdownOptions' => ['class' => 'float-right'],
                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
                'deleteOptions' => ['title' => 'Delete', 'data-toggle' => 'tooltip'],
                'headerOptions' => ['class' => 'kartik-sheet-style'],
            ],
    ];?>
    <?= GridView::widget([
        'id' => 'grid-table',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto '],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'toolbar' => [
            [
                'content'=>
                    Html::a('Delete selected', ['#'], [
                        'class' => 'btn btn-success align-items-start justify-content-end',
                        'onclick' => 'list = $("#grid-table").yiiGridView("getSelectedRows");
                                    model = "Modem";
                                    $.post("/ajax/delete?list="+list+"&model="+model,
                                     function(data){console.log(data);}
                                    );',
                    ]),
            ],
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
         'panel' => [
             'type' => GridView::TYPE_DEFAULT,
             'heading' => $this->title,
         ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10],
        'itemLabelSingle' => 'modem',
        'itemLabelPlural' => 'modems'
    ]);?>
</div>
