<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $key */
/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $server array*/
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modem-form container-fluid">
    <?php $form = ActiveForm::begin([
            'action' => Url::to(['/config/modem/update','model' => $model::className(), 'id' => $model->id]),
            'options' => ['method' => 'post', 'data-pjax' => true, 'onsubmit' => 'false'],
    ]); ?>
    <?= Html::a('Reset Ip', ['/proxy/restart-modem', 'id' => $model->id], ['class' => 'btn btn-secondary']);?>
    <div class="form-row">
        <?= $form->field($model, 'ip', ['options' => ['class' => 'form-group col-auto']])->textInput(['class' => 'form-control mb-2']) ?>
        <?= $form->field($model, 'gateway', ['options' => ['class' => 'form-group col-auto']])->textInput(['class' => 'form-control mb-2']) ?>
        <?= $form->field($model, 'provider_id', ['options' => ['class' => 'form-group col-auto']])->
        dropDownList(ArrayHelper::map(\app\models\Provider::find()->all(), 'id', 'name'), ['class' => 'form-control mb-2']) ?>
        <?= $form->field($model, 'server_id', ['options' => ['class' => 'form-group col-auto']])->
        dropDownList([$server['id'] => $server['ext_ip']], ['class' => 'form-control mb-2']) ?>
        <?= $form->field($model, 'reconnect', ['options' => ['class' => 'form-group col-1']])->textInput(['type' => 'number', 'min'=>'0']) ?>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>