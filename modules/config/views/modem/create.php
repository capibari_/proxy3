<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Modem */

$this->title = 'Create Modem';
$this->params['breadcrumbs'][] = ['label' => 'Modems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modem-create container-fluid">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
