<?php

use app\models\Modem;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modem-form container-fluid">
    <?php $form = ActiveForm::begin(); ?>
        <div class="form-row align-items-center">
            <div class="form-row">
                <?= $form->field($model, 'ip', ['options' => ['class' => 'form-group col-auto']])->textInput(['class' => 'form-control mb-2']) ?>
                <?= $form->field($model, 'gateway', ['options' => ['class' => 'form-group col-auto']])->textInput(['class' => 'form-control mb-2']) ?>
                <?= $form->field($model, 'provider_id', ['options' => ['class' => 'form-group col-auto']])->
                dropDownList(ArrayHelper::map(\app\models\Provider::find()->all(), 'id', 'name'), ['class' => 'form-control mb-2']) ?>
                <?= $form->field($model, 'reconnect', ['options' => ['class' => 'form-group col-auto']])->textInput(['type' => 'number', 'min'=>'0']) ?>
                <?= $form->field($model, 'server_id', ['options' => ['class' => 'form-group col-auto']])->
                dropDownList(ArrayHelper::map(\app\models\Server::find()->all(), 'id', 'ext_ip'), ['class' => 'form-control mb-2']) ?>
            </div>
            <div class="col-auto">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-2']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
