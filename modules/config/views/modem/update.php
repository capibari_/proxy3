<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Modem */

$this->title = 'Update Modem: ' . $model->ip;
$this->params['breadcrumbs'][] = ['label' => 'Modems', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="modem-update container-fluid">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
