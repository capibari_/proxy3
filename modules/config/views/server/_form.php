<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $form \yii\widgets\ActiveForm */
?>

<div class="server-form container-fluid">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'servername')->textInput()?>
    <?= $form->field($model, 'ext_ip')->textInput()?>
    <?= $form->field($model, 'int_ip')->textInput()?>
    <?= $form->field($model, 'path')->textInput()?>
    <?= $form->field($model, 'bash_folder')->textInput()?>
    <?= $form->field($model, 'log_folder')->textInput()?>
    <?= $form->field($model, 'configFile')->textInput()?>
    <?= $form->field($model, 'os', ['options' => ['class' => 'form-group col-auto']])->
    dropDownList(['win' => 'win', 'ubuntu' => 'ubuntu'], ['class' => 'form-control mb-2']) ?>
    <?= $form->field($model, 'config')->textArea()?>
    <?= $form->field($model, 'cron')->textArea()?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
