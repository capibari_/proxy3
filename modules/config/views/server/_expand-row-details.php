<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $key */
/* @var $this yii\web\View */
/* @var $model app\models\Modem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="server-form">
    <?php $form = ActiveForm::begin([
            'action' => Url::to(['/config/server/update', 'model' => $model::className(), 'id' => $model->id]),
            'options' => ['method' => 'post', 'data-pjax' => true, 'onsubmit' => 'false', ],
    ]); ?>
    <div class="form-row align-items-center">
        <?= $form->field($model, 'servername')->textInput()?>
        <?= $form->field($model, 'ext_ip')->textInput()?>
        <?= $form->field($model, 'int_ip')->textInput()?>
        <?= $form->field($model, 'configFile')->textInput()?>
        <?= $form->field($model, 'os', ['options' => ['class' => 'form-group col-auto']])->
        dropDownList(['win' => 'win', 'ubuntu' => 'ubuntu'], ['class' => 'form-control']) ?>
        <div>
            <?= Html::submitButton('Save', ['class' => 'btn btn-success mt-3']) ?>
        </div>
    </div>
    <div class="form-row align-items-center">
        <?= $form->field($model, 'path')->textInput()?>
        <?= $form->field($model, 'bash_folder')->textInput()?>
        <?= $form->field($model, 'log_folder')->textInput()?>
    </div>
    <?= $form->field($model, 'config')->textArea(['rows' => 10])?>
    <?= $form->field($model, 'cron')->textArea(['rows' => 10])?>
    <?php ActiveForm::end(); ?>
</div>