<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Add Server';
$this->params['breadcrumbs'][] = ['label' => 'Server list', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="server-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
