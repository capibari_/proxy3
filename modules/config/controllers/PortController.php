<?php

namespace app\modules\config\controllers;

use app\models\Server;
use app\models\Modem;
use Yii;
use app\models\Port;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class PortController extends \app\controllers\DefaultController
{
    public function actionIndex()
    {
        $array = [];
        $keys = [];
        $port_query = Port::find()->all();
        $server_list = Server::find()->asArray()->all();
        $modem_list = Modem::find()->asArray()->all();
        $port_list  = ArrayHelper::toArray($port_query, [
            'app\models\Port' => [
                'id',
                'modem_id',
                'number',
                'working',
                'description'
            ],
        ]);

        foreach ($port_list as $port) {
            array_push($keys, $port['id']);
            $modem = array_search($port['modem_id'], array_column($modem_list, 'id'));
            $server_id = $modem_list[$modem]['server_id'];
            $server = array_search($server_id, array_column($server_list, 'id'));

            array_push($array, [
                'id' => $port['id'],
                'number' => $port['number'],
                'modem_ip' => $modem_list[$modem]['ip'],
                'server_ip' => $server_list[$server]['ext_ip'],
                'workin' => $port['working'],
                'description' => $port['description'],
                'model' => $port_query,
                'modem' => $modem_list[$modem],
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $array,
            'keys' => $keys,
            'sort' => [
                'attributes' => ['id', 'number', 'modem', 'server', 'working'],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Port();
        if($form = Yii::$app->request->post()){
            $modem = Modem::find()->where(['id' => $form['Port']['modem_id']])->one();
            $form['Port']['server_id'] = $modem->server->id;
            $range = ($form['range'] > $form['Port']['number']) ? ($form['range'] - $form['Port']['number']) : 1;
            while ($range >= 0){
                $port = Port::find()->where(['number' => $form['Port']['number'], 'server_id' => $modem->server->id])->one();
                if(!$port){$model->load($form);
                    if($model->save()) {
                        $model = new Port();
                        $form['Port']['number']++;
                    }
                }
                $range--;
            }

            return $this->redirect(['index']);

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
        'model' => $model,
    ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Port::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
