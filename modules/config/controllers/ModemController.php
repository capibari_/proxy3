<?php

namespace app\modules\config\controllers;

use app\components\Debug;
use app\models\Provider;
use app\models\Server;
use http\Client;
use Yii;
use app\models\Modem;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


class ModemController extends \app\controllers\DefaultController
{
    public function actionIndex()
    {
        $array = [];
        $keys = [];
        $modem_query = Modem::find()->all();
        $modem_list  = ArrayHelper::toArray($modem_query, [
            'app\models\Modem' => [
                'id',
                'ip',
                'reconnect',
                'server_id',
                'provider_id'
            ],
        ]);
        $server_list = Server::find()->asArray()->all();
        $provider_list = Provider::find()->asArray()->all();

        foreach ($modem_list as $modem) {
            $server = array_search($modem['server_id'], array_column($server_list, 'id'));
            $provider = array_search($modem['provider_id'], array_column($provider_list, 'id'));
            array_push($keys, $modem['id']);
            array_push($array, [
                'id' => $modem['id'],
                'ip' => $modem['ip'],
                'reconnect' => $modem['reconnect'],
                'provider' => $provider_list[$provider]['name'],
                'server_ip' => $server_list[$server]['ext_ip'],
                'server_name' => $server_list[$server]['servername'],
                'server' => $server_list[$server],
                'model' => $modem_query,
            ]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $array,
            'keys' => $keys,
            'sort' => [
                'attributes' => ['id', 'ip', 'server_ip', 'server_name', 'reconnect', 'provider'],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Modem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Modem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
