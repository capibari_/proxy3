<?php


namespace app\modules\config\controllers;

use app\controllers\DefaultController;
use app\models\Server;

class MainController extends DefaultController
{
    public function actionIndex()
    {
        $data = [];
        foreach (Server::find()->all() as $server) {
            $data[$server->ext_ip] = [];

            foreach ($server->modems as $modem){
                $data[$server->ext_ip][$modem->ip] = [];
                foreach ($modem->ports as $port){
                    array_push($data[$server->ext_ip][$modem->ip], $port->number);
                }
            }
        }

        return $this->render('index', [
            'data' => $data,
        ]);
    }
}