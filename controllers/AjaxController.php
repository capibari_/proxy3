<?php


namespace app\controllers;

use app\components\Debug;
use app\components\PortList;
use app\models\Rules;
use app\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class AjaxController extends \app\controllers\DefaultController
{
    public function actionUpdate($model, $id)
    {
        $model = self::model($model);
        $model = $this->findModel($model, $id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/admin/'.$this->getClass($model).'/index']);
        }
        return $this->redirect(['/admin/'.$this->getClass($model).'/index']);
    }

    public function actionDelete($model, $list)
    {
        $model = self::model($model);
        $result = explode(',', $list);
        foreach ($result as $id) {
            if (!$this->findModel($model, $id)->delete()) {
                return 'Error in DelAjax';
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionRuleList()
    {
        $array = ArrayHelper::map(Rules::find()->all(), 'id', 'name');
        $data = '<option value="0">* * * *</option>';
        foreach ($array as $key => $rule){
            $data .= sprintf('<option value=%s>%s</option>', $key, $rule);
        }
        return $data;
    }

    public function actionPortList($rules)
    {
//        Debug::dump(PortList::getList(json_decode($rules)));
        return json_encode(PortList::getList(json_decode($rules)));
    }

    public function actionUser($id)
    {
        $user = User::findOne($id);
        return json_encode([
            'email' => $user['email'],
            'user_ip' => $user['user_ip']
        ]);
    }

    private static function model($model)
    {
        if(count(explode('\\',$model))<2){
            $model = 'app\models\\'.$model;
        }
        return $model;
    }

    private function getClass($model)
    {
        $class = explode('\\',$model::className());
        return mb_strtolower($class[count($class)-1]);
    }

    private function findModel($model, $id)
    {
        if (($model = $model::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}