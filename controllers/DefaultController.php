<?php

namespace app\controllers;

use app\components\Proxy;
use yii\web\Controller;

class DefaultController extends Controller
{
    protected function debug($data)
    {
        echo '<pre>';
        var_dump($data);
        die;
    }

    protected function sendConfig(array $servers)
    {
        foreach ($servers as $server) {
            $proxy = new Proxy($server);
            if($proxy->sendConfig() && $proxy->restartProxy()){
                return true;
            }
            else false;
        }
    }
}