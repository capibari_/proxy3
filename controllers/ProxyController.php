<?php

namespace app\controllers;

use app\components\Proxy;
use app\models\Modem;
use Yii;

class ProxyController extends DefaultController
{
    public function actionSendConfig($id)
    {
        $proxy = new Proxy($id);
        $proxy->sendConfig();

        Yii::$app->response->redirect(Yii::$app->request->referrer);
    }

    public function actionRestartService($id)
    {
        $proxy = new Proxy($id);
        $proxy->restartProxy();

        Yii::$app->response->redirect(Yii::$app->request->referrer);
    }

    public function actionSendCron($id)
    {
        $proxy = new Proxy($id);
        $proxy->sendCron();

        Yii::$app->response->redirect(Yii::$app->request->referrer);
    }

    public function actionRestartCron($id)
    {
        $proxy = new Proxy($id);
        $proxy->restartCron();

        Yii::$app->response->redirect(Yii::$app->request->referrer);
    }

    public function actionRestartModem($id)
    {
        $modem = Modem::find()->where(['id' => $id])->with('server')->asArray()->one();
        $proxy = new Proxy($modem['server']['id']);
        $proxy->resetModem($modem);

        Yii::$app->response->redirect(Yii::$app->request->referrer);
    }

}