<?php

use yii\db\Migration;

/**
 * Class m200423_120546_change_columns_in_order_tabler
 */
class m200423_120546_change_columns_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'proxy_type', $this->string());
        $this->addColumn('{{%order}}', 'auth_type', $this->string());
        $this->dropColumn('{{%order}}', 'token');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200423_120546_change_columns_in_order_tabler cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200423_120546_change_columns_in_order_tabler cannot be reverted.\n";

        return false;
    }
    */
}
