<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%port}}`.
 */
class m200203_021940_create_port_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%port}}', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull(),
            'modem_id' => $this->integer()->notNull(),
            'description' => $this->text(),
            'working' => $this->boolean()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%port}}');
    }
}
