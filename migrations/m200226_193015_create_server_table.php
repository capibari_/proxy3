<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%config}}`.
 */
class m200226_193015_create_server_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%server}}', [
            'id' => $this->primaryKey(),
            'ext_ip' => $this->string(),
            'int_ip' => $this->string(),
            'path' => $this->string(),
            'configFile' => $this->string(),
            'config' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%server}}');
    }
}
