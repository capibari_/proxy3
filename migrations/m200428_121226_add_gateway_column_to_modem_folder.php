<?php

use yii\db\Migration;

/**
 * Class m200428_121226_add_gateway_column_to_modem_folder
 */
class m200428_121226_add_gateway_column_to_modem_folder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%modem}}', 'gateway', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200428_121226_add_gateway_column_to_modem_folder cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200428_121226_add_gateway_column_to_modem_folder cannot be reverted.\n";

        return false;
    }
    */
}
