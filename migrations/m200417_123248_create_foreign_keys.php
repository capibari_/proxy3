<?php

use yii\db\Migration;

/**
 * Class m200417_123248_create_foreign_keys
 */
class m200417_123248_create_foreign_keys extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk-modem-server', '{{%modem}}', 'server_id', '{{%server}}', 'id','CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-port-server', '{{%port}}', 'server_id', '{{%server}}', 'id','CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-port-modem', '{{%port}}', 'modem_id', '{{%modem}}', 'id','CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-order-port', '{{%order}}', 'port_id', '{{%port}}', 'id','CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-order-user', '{{%order}}', 'user_id', '{{%user}}', 'id','CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-firewall-order', '{{%firewall}}', 'order_id', '{{%order}}', 'id','CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-firewall-rule', '{{%firewall}}', 'rule_id', '{{%rules}}', 'id','CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200417_123248_create_foreign_keys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200417_123248_create_foreign_keys cannot be reverted.\n";

        return false;
    }
    */
}
