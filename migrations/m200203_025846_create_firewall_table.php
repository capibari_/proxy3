<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rules}}`.
 */
class m200203_025846_create_firewall_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%firewall}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'rule_id' => $this->integer(),
            'description' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%firewall}}');
    }
}
