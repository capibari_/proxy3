<?php

use yii\db\Migration;

/**
 * Class m200417_121842_create_indexes
 */
class m200417_121842_create_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-config-ext_ip-servername', '{{%server}}', 'ext_ip');
        $this->createIndex('idx-config-servername', '{{%server}}', 'servername');
        $this->createIndex('idx-modem-server_id', '{{%modem}}', 'server_id');
        $this->createIndex('idx-port-server_id', '{{%port}}', 'server_id');
        $this->createIndex('idx-port-modem_id', '{{%port}}', 'modem_id');
        $this->createIndex('idx-user-username', '{{%user}}', 'username');
        $this->createIndex('idx-order-port_id', '{{%order}}',  'port_id');
        $this->createIndex('idx-order-user_id', '{{%order}}', 'user_id');
        $this->createIndex('idx-firewall-rule_id', '{{%firewall}}', 'rule_id');
        $this->createIndex('idx-firewall-order_id', '{{%firewall}}', 'order_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200417_121842_create_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200417_121842_create_indexes cannot be reverted.\n";

        return false;
    }
    */
}
