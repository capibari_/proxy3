<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%server}}`.
 */
class m200427_193553_add_bash_folder_log_folder_columns_to_server_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%server}}', 'bash_folder', $this->string());
        $this->addColumn('{{%server}}', 'log_folder', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
