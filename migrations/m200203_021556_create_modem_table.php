<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%modem}}`.
 */
class m200203_021556_create_modem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%modem}}', [
            'id' => $this->primaryKey(),
            'ip' => $this->string()->notNull(),
            'description' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%modem}}');
    }
}
