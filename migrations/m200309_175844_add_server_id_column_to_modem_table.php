<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%modem}}`.
 */
class m200309_175844_add_server_id_column_to_modem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%modem}}', 'server_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%modem}}', 'server_id');
    }
}
