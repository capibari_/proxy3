<?php

use yii\db\Migration;

/**
 * Class m200423_121234_change_columns_in_user_tablee
 */
class m200423_121234_change_columns_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'token', $this->string());
        $this->addColumn('{{%user}}', 'user_ip', $this->string());
        $this->dropColumn('{{%user}}', 'password');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200423_121234_change_columns_in_user_tablee cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200423_121234_change_columns_in_user_tablee cannot be reverted.\n";

        return false;
    }
    */
}
