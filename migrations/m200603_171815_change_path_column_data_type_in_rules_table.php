<?php

use yii\db\Migration;

/**
 * Class m200603_171815_change_path_column_data_type_in_rules_table
 */
class m200603_171815_change_path_column_data_type_in_rules_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('rules', 'path', 'text');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('rules', 'path', 'string' );

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200603_171815_change_path_column_data_type_in_rules_table cannot be reverted.\n";

        return false;
    }
    */
}
