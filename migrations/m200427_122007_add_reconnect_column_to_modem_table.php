<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%modem}}`.
 */
class m200427_122007_add_reconnect_column_to_modem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%modem}}', 'reconnect', $this->integer()->defaultValue(5)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
