<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%port}}`.
 */
class m200312_153007_add_server_id_column_to_port_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%port}}', 'server_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
