<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%modem}}`.
 */
class m200507_124732_add_provider_id_column_to_modem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%modem}}', 'provider_id', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
