<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%allow}}`.
 */
class m200203_025344_create_rules_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rules}}', [
            'id' => $this->primaryKey(),
            'permission' => $this->string()->notNull(),
            'path' => $this->string(),
            'name' => $this->string(),
            'use_limit' => $this->integer(),
            'description' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rules}}');
    }
}
