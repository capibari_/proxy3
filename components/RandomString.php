<?php

namespace app\components;

class RandomString
{

    public static function generate($length = 10)
    {
        $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        // Количество символов в пароле.

        $size=StrLen($chars)-1;
        // Определяем пустую переменную, в которую и будем записывать символы.

        $string=null;
        // Создаём пароль.

        while($length--){
            $string.=$chars[rand(0,$size)];
        }

        return $string;
    }

}