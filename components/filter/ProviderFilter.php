<?php

namespace app\components\filter;

use app\components\Debug;

class ProviderFilter extends DefaultFilter implements FilterInterface
{
    public function __construct(array $rules, string $filter = null)
    {
        parent::__construct($rules, $filter);
    }

    public function filter($array)
    {
        if($this->rules["provider"]){
            foreach ($array as $key => $port){
                if($port['modem']['provider_id'] != $this->rules["provider"]){
                    unset($array[$key]);
                }
            }
        }

        return parent::filter($array);
    }
}