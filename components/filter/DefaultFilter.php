<?php

namespace app\components\filter;

use app\components\Stringer;

abstract class DefaultFilter implements FilterInterface
{
    protected $filter;
    protected $rules;
    protected $next;

    public function __construct(array $rules, string $filter = null)
    {
        $this->filter = $filter;
        $this->rules = $this->setRules($rules);
    }

    public function next()
    {
        $class = 'app\components\filter\\'.Stringer::className(array_key_first($this->rules)).'Filter';
        if(class_exists($class)){
            return new $class($this->rules, array_key_first($this->rules));
        }
    }

    public function filter($array)
    {
        if($this->filter){
            unset($this->rules[$this->filter]);
        }
        if(count($this->rules)){
            if($next = $this->next()){
                $array = $next->filter($array);
            }
        }

        return $array;
    }

    protected function setRules($rules)
    {
        foreach ($rules as $key => $rule){
            if(
                !class_exists('app\components\filter\\'.Stringer::className($key).'Filter')
                ||
                !$rule
            ){
                unset($rules[$key]);
            }
        }

        return $rules;
    }
}