<?php

namespace app\components\filter;

class Filter extends DefaultFilter implements FilterInterface
{
    public function __construct(array $rules, string $filter = null)
    {
        parent::__construct($rules, $filter);
    }

    public function filter($array)
    {
        return parent::filter($array);
    }
}