<?php

namespace app\components\filter;

use app\models\Firewall;
use app\models\Order;
use app\models\Rules;

class LimitFilter extends DefaultFilter implements FilterInterface
{
    public function __construct(array $rules, string $filter = null)
    {
        parent::__construct($rules, $filter);
    }

    public function filter($array)
    {
        $limit = $this->getLimit($array, $this->rules[$this->filter]);
        foreach ($this->rules[$this->filter] as $rule){
            if(!$rule)continue;
            foreach($array as $key => $port){
                if($limit[$port['modem']['id']][$rule] < 1){
                    unset($array[$key]);
                } else {
                    $limit[$port['modem']['id']][$rule]--;
                }
            }
        }
        return parent::filter($array);
    }

    private function getLimit($array, $rules)
    {
        $limit = [];
        foreach ($rules as $rule){
            if($rule){
                $ruleLimit = Rules::find()->where(['id' => $rule])->asArray()->one()['use_limit'];
                foreach ($array as $port) {
                    $limit[$port['modem']['id']][$rule] = $ruleLimit;
                    foreach(Firewall::find()->asArray()->all() as $used){
                        $order = Order::find()->where(['id' => $used['order_id']])->with('port')->asArray()->one();
                        if($port['modem_id'] == $order['port']['modem_id'] && $limit[$port['modem_id']][$used['rule_id']]) {
                            $limit[$port['modem_id']][$used['rule_id']]--;
                        }
                    }
                }
            }
        }
        return $limit;
    }
}