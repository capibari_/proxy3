<?php

namespace app\components\filter;

interface FilterInterface
{
    public function filter($array);

    public function next();
}