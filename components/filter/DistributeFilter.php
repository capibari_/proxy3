<?php


namespace app\components\filter;

use app\models\Modem;
use app\models\Order;

class DistributeFilter extends DefaultFilter implements FilterInterface
{

    public function __construct(array $rules, string $filter = null)
    {
        parent::__construct($rules, $filter);
        $this->countPorts();
    }

    public function filter($array)
    {
        $result = [];

        while ($array){
            foreach ($this->countPorts() as $modem => $count) {
                foreach ($array as $key => $port){
                    if($modem == $port['modem_id']){
                        array_push($result, $port);
                        unset($array[$key]);
                        break;
                    }
                }
            }
        }

        return parent::filter($result);
    }

    private function countPorts()
    {
        $count = [];

        foreach(Modem::find()->with('ports')->asArray()->all() as $modem){
            $count[$modem['id']] = count($modem['ports']);
        }
        foreach(Order::find()->with('port.modem')->asArray()->all() as $order){
            $count[$order['port']['modem']['id']]--;
        }

        arsort($count);
        return $count;
    }
}