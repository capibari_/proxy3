<?php

namespace app\components\filter;


class ReconnectFilter extends DefaultFilter implements FilterInterface
{
    public function __construct(array $rules, string $filter = null)
    {
        parent::__construct($rules, $filter);
    }

    public function filter($array)
    {
        if($this->rules["reconnect"]){
            foreach ($array as $key => $port){
                if($port['modem']['reconnect'] != $this->rules["reconnect"]){
                    unset($array[$key]);
                }
            }
        }
        return parent::filter($array);
    }
}