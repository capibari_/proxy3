<?php

namespace app\components\filter;

class InUseFilter extends DefaultFilter implements FilterInterface
{
    public function __construct(array $rules, string $filter = null)
    {
        parent::__construct($rules, $filter);
    }

    public function filter($array)
    {
        foreach($array as $key => $port){
            if($port['order']){
                unset($array[$key]);
            }
        }

        return parent::filter($array);
    }
}