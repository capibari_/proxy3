<?php


namespace app\components;


class Debug
{
    public static function dump($data)
    {
        echo '<pre>';
        var_dump($data);
        die;
    }
}