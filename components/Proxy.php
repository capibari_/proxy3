<?php

namespace app\components;

use app\components\proxy\Config;
use app\components\proxy\Cron;
use app\models\Modem;
use app\models\Server;
use app\models\Order;
use yii\httpclient\Client;

class Proxy
{
    private $config;
    private $orders;
    private $server;
    private $modems;
    private $data;
    private $cron;


    public function __construct($id)
    {
        $this->server = Server::find()->where(['id' => $id])->asArray()->one();
        $this->modems = Modem::find()->where(['server_id' => $id])->asArray()->all();
        $this->orders = Order::find()->with('port', 'rules', 'user', 'rules.rule', 'port.modem')->asArray()->all();
        $this->config = $this->setConfig();
        $this->cron = $this->setCron();
        $this->data = $this->setData();
    }

    public function restartProxy()
    {
        return $this->request('proxy/restart-service', $this->data);
    }

    public function resetModem($modem)
    {
        $this->data['modem'] = $modem['gateway'];
        return $this->request('modem/reset', $this->data);
    }

    public function sendConfig()
    {
        return $this->request('proxy/set-config', $this->data);
    }

    public function sendCron()
    {
        return $this->request('cron/set-cron', $this->data);
    }

    public function restartCron()
    {
        return $this->request('cron/restart-cron', $this->data);
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function getCron()
    {
        return $this->cron;
    }

    private function setConfig()
    {
        $config = new Config($this->server, $this->orders);
        return $config->generate();
    }

    private function setCron()
    {
        $cron = new Cron($this->server, $this->modems);
        return $cron->generate();
    }

    private function setData()
    {
        $data = [
            'os' => $this->server['os'],
            'config' => $this->config,
            'path' => $this->server['path'],
            'file' => $this->server['configFile'],
            'cron' => $this->cron,
        ];

        return $data;
    }

    private function request($uri, $data, $method = 'POST')
    {
        $client = new Client(['baseUrl' => sprintf('http://%s', $this->server['ext_ip'])]);
        $response = $client->createRequest()
            ->setUrl($uri)
            ->setMethod($method)
            ->setData($data)
            ->send();

        return $response->content;
    }
}