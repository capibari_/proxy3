<?php


namespace app\components;


class Stringer
{
    public static function setEol($os = 'ubuntu')
    {
        $eol = PHP_EOL;

        switch ($os) {
            case 'win':
                $eol =  "\r\n";
                break;
            case 'ubuntu':
                $eol = "\n";
                break;
        }

        return $eol;
    }

    public static function server($ip)
    {
        return explode(':', $ip)[0];
    }

    public static function port(array $config, $type = 0)
    {
        switch ($type) {
            case 0:
                $delimer = ":";
                break;
            case 1:
                $delimer = "@";
                break;
        }

        $ip = self::server($config['ip']);

        return sprintf('%s:%s%s%s:%s%s',
            $ip, $config['port'], $delimer, $config['user'], $config['token'], self::setEol()
        );
    }

    public static function className($name)
    {
        $class ='';

        foreach(explode('_', $name) as $key => $value){
            $class .= ucfirst($value);
        }

        return $class;
    }

}