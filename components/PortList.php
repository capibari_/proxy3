<?php

namespace app\components;

use app\components\filter\Filter;
use app\models\Port;

class PortList
{
    public static function getList($rules)
    {
        $list = [];
        foreach(self::portList($rules) as $port){
            $list[] = [
                'id' => $port['id'],
                'port' => $port['modem']['server']['ext_ip'].':'.$port['number'],
                'provider' => $port['modem']['provider_id'],
                'reconnect' => $port['modem']['reconnect']
            ];
        }

        return $list;
    }

    private static function portList($rules)
    {
        $filter = new Filter((array)$rules);
        return $filter->filter(
            Port::find()->with('modem','order', 'order.rules', 'modem.server')->indexBy('id')->asArray()->all()
        );
    }
}