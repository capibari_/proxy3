<?php


namespace app\components\proxy;


use app\components\Debug;
use app\components\Stringer;

class Cron
{
    protected $server;
    protected $modems;
    protected $eol;

    public function __construct($server, $modems)
    {
        $this->server = $server;
        $this->modems = $modems;
        $this->eol = Stringer::setEol($this->server['os']);
    }

    public function generate()
    {
        $cron = $this->server['cron'].$this->eol;
        $cron .= $this->SetTaskList();

        return $cron;
    }

    private function SetTaskList()
    {
        $list = '';

        foreach ($this->modems as $modem) {
            $list .= $this->setTask($modem).$this->eol;
        }

        return $list;
    }

    private function setTask($modem)
    {
        $script = sprintf('%s/reсonnect.sh', $this->server['bash_folder']);
        $log = sprintf('%s/%s.log', $this->server['log_folder'], $modem['ip']);
        $task = sprintf('*/%s * * * * /usr/bin/flock -w 0 /var/run/%s.lock %s -r 4G  -i %s >>%s 2>%s',
            $modem['reconnect'], $modem['ip'], $script, $modem['gateway'], $log, $log);

        return $task;
    }
}