<?php


namespace app\components\proxy;


use app\components\Debug;
use app\components\Stringer;

class Config
{
    protected $server;
    protected $orders;
    protected $eol;

    public function __construct($server, $orders)
    {
        $this->server = $server;
        $this->orders = $orders;
        $this->eol = Stringer::setEol($this->server['os']);
    }

    public function generate()
    {
        $config = $this->stringMonitor().$this->eol;
        $config .= $this->server['config'].$this->eol;
        $config .= $this->setOrders();

        return $config;
    }

    private function stringMonitor()
    {
        return 'monitor '.$this->server['path'].'/'.$this->server['configFile'];
    }

    private function setOrders()
    {
        $orders = '';

        foreach ($this->orders as $order) {
            if(($order['activate']) < time() && ($order['end']) > time() && $order['is_activate'] && $order['user']['status']){
                $orders .= $this->generateOrder($order).'flush'.$this->eol;
            }
        }

        return $orders;
    }

    private function generateOrder($order)
    {
        $result = $this->stringAuth($order['user'],  $order['auth_type'], $order['rules']).$this->eol;

        $result .= $this->stringProxy($order['proxy_type'], $order['port']['number'], $order['port']['modem']['ip']).$this->eol;

        return $result;
    }

    private function stringProxy($type, $port, $modem)
    {
        return sprintf("%s -n -a -p%s -i%s -e%s", $type ,$port, $this->server['int_ip'], $modem);
    }

    private function generateRules($rules, $user)
    {
        $result = '';
        foreach ($rules as $rule){
            if($rule['rule']){
                $permission = ($rule->permission) ? 'allow' : 'deny';
                $result .= sprintf('%s %s * "%s"', $permission, $user, str_replace(' ', '',$rule['rule']['path'])).$this->eol;
            } else {
                $result .= sprintf('allow %s', $user).$this->eol;
            }
        }

        return $result;
    }

    private function stringAuth($user, $auth_type, $rules)
    {
        if($auth_type == 'ip'){
            $auth = 'auth iponly'.$this->eol;
            $auth .= $this->generateRules($rules, $user['username']);
            $auth .= 'allow * '.$user['user_ip'];
        } elseif($auth_type == 'login'){
            $auth = 'auth strong'.$this->eol;
            $auth .= 'users '.$user['username'].':CL:'.$user['token'].$this->eol;
            $auth .= $this->generateRules($rules, $user['username']);
            $auth .= 'allow '.$user['username'];
        }

        return $auth;
    }
}