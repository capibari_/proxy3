let multiple = $("input#generatemodel-multiple");

function execute() {
    button();
    let rules = {'limit':{}};
    $("input.rules-checkbox:checked").each(function () {
        rules[this.id] = this.id;
    });
    $("select#provider").each(function(){
        rules[this.id] = $(this).val();
    });
    $("select#reconnect").each(function(){
        rules[this.id] = $(this).val();
    });

    $("select.rule option:selected").each(function(){
        rules['limit'][$(this).val()] = $(this).val();
    });
    $.post("/ajax/port-list?rules="+JSON.stringify(rules), function (data) {
        let ports = JSON.parse(data);

        if(multiple.val() > Object.values(ports).length){
            multiple.val(Object.values(ports).length);
        }
        multiple.prop("max", Object.values(ports).length);
        $("div#avalible_port").html("<b>" + multiple.val()   + "/" + Object.values(ports).length + "</b>");
        $("select#generatemodel-ports").html(getPorts(multiple.val(), ports));
});
}

function button() {
    if(multiple.val() < 1){
        $("button#sendForm").prop('disabled', true);
    }else{
        $("button#sendForm").prop('disabled', false);
    }
}

function getPorts(multiple, ports) {
    let list = '';
    for(id in ports){
        (port(id));
        if(multiple < 1) break;
        list += port(id).outerHTML;
        multiple--;
    }

    function  port(port) {
        let element = document.createElement("option");
        element.setAttribute("value", ports[port]['id']);
        element.setAttribute("selected", "selected");
        element.innerText = ports[port]['port'];
        return element;
    }

    return list;
}

function checkAuthType()
{
    if($("select#order-auth_type").val() == 'ip' && !$("input#user-user_ip").val()){
        $("button#sendForm").prop('disabled', true);
        $("div#error_status").attr('class', 'col alert-danger');
        $("div#error_status").html('wrong ip');
    } else {
        $("button#sendForm").prop('disabled', false);
        $("div#error_status").attr('class','col');
        $("div#error_status").empty();
    }
}

$(document).ready(function(){
    let i = $('.rules').length;
    button();

    function form(i, data) {
        let field = document.createElement("div");
        field.setAttribute("class", "field_rule form-group col-auto field-rules-permission required");
        field.setAttribute("id", "rule_" + i);

        let feedback = document.createElement("div");
        feedback.setAttribute("class", "invalid-feedback");

        let select = document.createElement("select");
        select.setAttribute("id", "select_rule_" + i);
        select.setAttribute("class", "rule form-control");
        select.setAttribute("name", "Rules["+i+"]");
        select.setAttribute("onchange", "execute()");
        select.innerHTML = data;

        field.append(select);
        field.append(feedback);

        return field;
    }

    $('input.rules-checkbox').on('change', function() {
        execute();
        checkAuthType();
    });
    $('select#reconnect').on('change', function() {
        execute();
        checkAuthType();
    });

    $('select#provider').on('change', function() {
        execute();
        checkAuthType();
    });

    document.querySelector("input#generatemodel-multiple").addEventListener('change', function () {
        execute();
        checkAuthType();
    });

    document.getElementById('add').addEventListener('click', function () {
        $.post("/ajax/rule-list", function(data) {
            $(form(i, data)).appendTo('.rules');
            i++;
        })
    });

    document.getElementById('remove').addEventListener('click', function () {
        $('.field_rule:last').remove();
        i--;
        execute();
    });

    document.getElementById('user-username').addEventListener('change', function () {
        $.post("/ajax/user?id="+$(this).val(), function(data) {
            data = JSON.parse(data);
            $("input#user-email").val(data['email']);
            $("input#user-user_ip").val(data['user_ip']);
        })
    });

    document.getElementById('order-auth_type').addEventListener('change', function () {
        checkAuthType();
    });

    document.getElementById('user-user_ip').addEventListener('change', function () {
        checkAuthType();
    });

});