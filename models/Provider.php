<?php


namespace app\models;


class Provider extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'provider';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Provider',
        ];
    }

    public function getModems()
    {
        return $this->hasMany(Modem::className(), ['provider_id' => 'id']);
    }
}
