<?php

namespace app\models;

use app\components\Debug;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class GenerateModel extends Model
{
    public $multiple;
    public $ports;
    public $distribute;
    public $in_use;
    public $provider;
    public $reconnect_time;


    public function __construct($multiple = 1, $config = [])
    {
        $this->multiple = $multiple;
        $this->provider = $this->setProvider();
        $this->reconnect_time = $this->setReconnect();
        $this->ports = Port::find()->all();
        parent::__construct($config);
    }

    private function setProvider()
    {
        return ['0'=>'All'] + ArrayHelper::map(Provider::find()->all(), 'id', 'name');
    }

    private function setReconnect()
    {
        return ['0'=>'All'] + ArrayHelper::map(Modem::find()->all(), 'reconnect', 'reconnect');
    }
}