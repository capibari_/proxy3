<?php

namespace app\models;

class Firewall extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'firewall';
    }


    public function rules()
    {
        return [
            [['order_id', 'rule_id'], 'required'],
            [['description'], 'string'],
            [['order_id', 'rule_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order',
            'rule_id' => 'Rule',
            'description' => 'Description',
        ];
    }

    public function getRule()
    {
        return $this->hasOne(Rules::className(), ['id' => 'rule_id']);
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
