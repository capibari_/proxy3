<?php

namespace app\models;

use app\components\RandomString;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

class User extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['username', 'token'], 'required'],
            [['username'], 'unique'],
            [['status'], 'integer'],
            [['description'], 'string'],
            [['username', 'token', 'user_ip'], 'string', 'max' => 255],
            [['email'], 'email']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'token' => 'Token',
            'user_ip' => 'User Ip',
            'status' => 'Status',
            'description' => 'Description',
        ];
    }

    public function userList()
    {
        $user_list = ArrayHelper::map(User::find()->all(), 'id', 'username');
        $user_list[0] = 'new';
        ksort($user_list);
        return $user_list;
    }

    public static function findByUsername($username)
    {
        return static::find()->where(['username' => $username])->one();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    public static function getUsernameAsArray()
    {
        return ArrayHelper::map(self::find()->select(['id', 'username'])->all(), 'id', 'username');
    }

    public static function getEmailAsArray()
    {
        return ArrayHelper::map(self::find()->select(['id', 'email'])->all(), 'id', 'username');
    }

    public function beforeDelete()
    {
        foreach ($this->orders as $order) $order->delete();
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function beforeSave($insert)
    {
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
