<?php

namespace app\models;

use app\models\Order;
use yii\base\Model;
use yii\data\ArrayDataProvider;


class OrderSearch extends Order
{
    public $email;
    public $ruleList;
    public $username;
    public $portNumber;
    public $serverName;
    public $modem;

    public function rules()
    {
        return [
            [['id', 'user_id', 'port_id', 'portNumber', 'modem'], 'integer'],
            [['is_activate'], 'boolean'],
            [['ruleList', 'username', 'email', 'serverName', 'activate', 'end'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Order::find();
        $query->andFilterWhere(['like', 'ruleList', $this->ruleList]);

        $this->load($params);

        $query->andFilterWhere([
            'id' => $this->id,
            'is_activate' => $this->is_activate,
//            'activate' => $this->activate,
//            'end' => $this->end
        ]);

        $data = $query->all();

        if($this->ruleList){
            $data = array_filter($data, function($item){
                return boolval(stristr($item->ruleList, $this->ruleList));
            });
        }
        if($this->serverName){
            $data = array_filter($data, function($item){
                return $item->port->server->id == $this->serverName;
            });
        }
        if($this->username){
            $data = array_filter($data, function($item){
                return $item->user->id == $this->username;
            });
        }
        if($this->email){
            $data = array_filter($data, function($item){
                return $item->user->id == $this->email;
            });
        }
        if($this->modem){
            $data = array_filter($data, function($item){
                return $item->port->modem->id == $this->modem;
            });
        }
        if($this->portNumber){
            $data = array_filter($data, function($item){
                return $item->port->number == $this->portNumber;
            });
        }


        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'key' => function ($model) {
                return $model->id;
            }
        ]);

        foreach ($this->rules() as $array){
            foreach ($array as $rules) {
                if(is_array($rules)){
                    foreach ($rules as $rule){
                        $dataProvider->sort->attributes[$rule] = [
                            'asc' => [$rule => SORT_ASC],
                            'desc' => [$rule => SORT_DESC],
                        ];
                    }
                }
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        return $dataProvider;
    }
}
