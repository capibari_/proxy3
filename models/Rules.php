<?php

namespace app\models;

use yii\helpers\ArrayHelper;

class Rules extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'rules';
    }

    public function rules()
    {
        return [
            [['permission', 'path', 'name'], 'required'],
            [['path', 'name','description'], 'string'],
            [['use_limit'], 'integer'],
            [['permission'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'permission' => 'Permission',
            'path' => 'Url',
            'name' => 'Name',
            'use_limit' => 'Limit',
            'description' => 'description'
        ];
    }

    public static function getRuleListAsArray()
    {
        return ArrayHelper::map(self::find()->select(['name', 'name'])->all(), 'name', 'name');
    }


    public function getConfig()
    {
        return $this->hasMany(Firewall::className(), ['rule_id' => 'id']);
    }


}
