<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="bg-light">
<?php $this->beginBody() ?>
<?php
NavBar::begin([
    'options' => [
        'class' => 'navbar navbar-expand fixed-top navbar-dark bg-dark',
    ],
    'innerContainerOptions' => [
        'class' => 'navbar',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav mr-auto'],
    'items' => [
        ['label' => 'Manage', 'url' => ['/manage/main/index']],
        ['label' => 'Config', 'url' => ['/config/main/index']],
    ],
]);
NavBar::end();
NavBar::begin([
    'options' => [
        'class' => 'navbar navbar-expand-sm navbar-dark bg-dark',
    ],
    'innerContainerOptions' => [
        'class' => 'navbar',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav mr-auto'],
    'items' => [
        ['label' => 'Config', 'url' => ['/config/main/index']],
        ['label' => 'Servers', 'url' => ['/config/server/index']],
        ['label' => 'Modems', 'url' => ['/config/modem/index']],
        ['label' => 'Ports', 'url' => ['/config/port/index']],
        ['label' => 'Providers', 'url' => ['/config/provider/index']],
    ],
]);
NavBar::end();
?>
<div class="wrap">
    <div class="container-fluid">
        <br>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>